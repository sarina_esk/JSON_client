#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include<QJsonDocument>
#include<QUdpSocket>
#include<QJsonObject>
class Client : public QObject
{
    Q_OBJECT
public:
    explicit Client(QObject *parent = nullptr);
    QJsonDocument doc();

signals:

public slots:
       void recieveData();
private:
       QUdpSocket *csocket;
       int sum;
       int count;
       float avg;

};

#endif // CLIENT_H
